-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

local background = display.newRect(240,1240,10000,10000);

local person = display.newCircle(display.contentCenterX, display.contentCenterX ,200);
person:setFillColor(0/255,0/255,0/255)


local borderleft = display.newRect(-800,500, 10,3600 );

borderleft:setFillColor(255/255,255/255,255/255,1 );

local borderright = display.newRect(1500,500, 10,3600 );

borderright:setFillColor(255/255,255/255,255/255,1 );

local bordertop = display.newRect(display.contentCenterX,-25,2500, 50);
bordertop:setFillColor(0/255,0/255,0/255,1 );

local platform = display.newRect( display.contentCenterX ,1170,10800,100);
platform:setFillColor(180/255,180/255,180/255,1 );


local physics = require("physics");

physics.start();
physics.setGravity(0,0);

physics.addBody(person,"dynamic",{bounce=1});
physics.addBody(platform,"static",{bounce=1});
physics.addBody(bordertop,"static",{bounce=1});
physics.addBody(borderleft,"static",{bounce=1});
physics.addBody(borderright,"static",{bounce=1});

